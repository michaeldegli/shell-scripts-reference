#!/bin/bash

user=$1
echo "Adding user $user ..."
sudo useradd $user -s /bin/bash -m -G sudo
echo $?
sudo passwd $user
echo $?
