#!/bin/bash

pushd /Users/michael/www/project > /dev/null

branch=$1
git fetch
git checkout "$branch"
git pull --ff-only

git checkout develop
git merge --ff-only $branch
git push


popd /dev/null > /dev/null
