#!/bin/bash

declare -a arr=("domain.ca"
"sub.domain.ca"
"another.domain.ca" 
)

generate_csr () {
for i in "${arr[@]}"
do
	d=$i
	domain=$(echo $d | tr -d ' ')

	openssl req \
	-new -newkey rsa:2048 -nodes \
	-subj "/CN=${domain}/O=Corporation/OU=Line of business/C=CA/ST=Ontario/L=Toronto" \
	-keyout "${domain}.key" -out "${domain}.csr"
done
}

view_csr_subject () {
	for i in "${arr[@]}"
	do
		SUBJECT=$(openssl req -in ${i}.csr -noout -subject)
		echo $SUBJECT
		done
}

sflag="" # subject
gflag="" # generate
rflag="" # remove certs
while getopts "rsgf:" flag; do
	case "${flag}" in
    	s) sflag="true" ;;
		g) gflag="true" ;;
    	r) rflag="true" ;;
		f) files="${OPTARG}" ;;
		*) error "Unexpected option ${flag}" ;;
	esac
done

if [ "$sflag" == "true" ]
then
	view_csr_subject
elif [ "$gflag" == "true" ]
then
	generate_csr
elif [ "$rflag" == "true" ]
then
	`rm -f *.domain.ca`
else
	echo "Usage: ./csr.sh (-s | -g | -r)"
	echo "-s displays the subject line of the csr files generated."
	echo "-g generate csr files from the array of domains in this file."
	echo "-r removes all generated csr files."
fi
